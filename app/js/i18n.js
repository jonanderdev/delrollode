'use strict';

angular
    .module('delrollode')
    .config(['$translateProvider', function ($translateProvider) {
        $translateProvider.translations('en', {
            APPNAME: 'DelRollode',
            SEARCH: 'Search',
            SEARCH_HISTORY: 'History',
            EVENTS: 'Events',
            GENRES: 'Genres',
            MOODS: 'Moods',
            
          });
         
        $translateProvider.translations('es', {
            APPNAME: 'DelRollode'
        });

        $translateProvider.preferredLanguage('en');

        $translateProvider.useSanitizeValueStrategy(null);

      }]);

