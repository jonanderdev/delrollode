'use strict';

angular
	.module('delrollode')
    .factory('DataService', [ '$http', '$log' , function ($http, $log) {

    	var data = {
    		mobileOrTable: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent), ///Check if we're in a mobile device
    		ios:  /iPhone|iPad|iPod/i.test(navigator.userAgent),
    		previousHash: '',
            currentLanguage: 'en'
    	};

    	return data;

    }]);

angular
    .module('delrollode')
    .factory('Services', [ '$http', '$log' , 'DataService', function ($http, $log, DataService) {
        return {
            //LASTFM service: http://www.lastfm.es/api
                lastfm: function(){
                    var data = {
                        apiKey: '9bdab0f268fcc4f822bc40181435e545'
                    }

                    return {
                        getInfo: function(artist, success, error) {
                            var url = 'http://ws.audioscrobbler.com/2.0/?callback=JSON_CALLBACK&method=artist.getinfo&lang='+DataService.currentLanguage+'&artist='+artist+'&api_key='+ data.apiKey +'&format=json';
                            $http.jsonp(url).success(function(data){
                                    if(data && data.artist){
                                        var artist = data.artist;
                                        success(artist);
                                    }else{
                                        error(data);
                                    }
                                });  
                        },
                        getSimilar: function(artist, success, error) {
                            var url = 'http://ws.audioscrobbler.com/2.0/?callback=JSON_CALLBACK&autocorrect=1&method=artist.getsimilar&lang='+DataService.currentLanguage+'&artist='+artist+'&api_key='+ data.apiKey +'&format=json';
                            $http.jsonp(url).success(function(data){
                                    if(data && data.similarartists && data.similarartists.artist){
                                        var similarArtists = data.similarartists.artist;
                                        success(similarArtists);
                                    }else{
                                        error(data);
                                    }
                                });
                        },
                        getByTag: function(tag, success, error){
                            var url = 'http://ws.audioscrobbler.com/2.0/?callback=JSON_CALLBACK&method=tag.gettopartists&tag='+tag+'&api_key='+ data.apiKey +'&format=json';
                            $http.jsonp(url).success(function(data){
                                    if(data && data.topartists && data.topartists.artist){
                                        var topartists = data.topartists.artist;
                                        success(topartists);
                                    }else{
                                        error(data);
                                    }
                                });
                            
                        }
                    }
                },

            //SPOTIFY service: 
                spotify: function(){

                }
        };

}]);

   

angular
	.module('delrollode')
    .factory('Music', [ '$http', '$log' , 'Services', 'DataService', function ($http, $log, Services, DataService) {

    	var data = {
    		searchedArtists: new Array(),
    		insertedArtists: new Array(),
    		checkingArtistIntro: 0, //To know which artist has been written uncorrectly
    		oneBadWrittenArtistName: false,
    		insertedArtistList: new Array(),
    		searchedArtistsList: new Array(),
			artistsFuncObtSimilarArt: new Array(),
			artistasFuncsQueue: new Array(),
			searchedArtistsInserted: 0,
            ARTIST_KEY: 'artist_key',
            ARTISTS_KEY: 'artists_key',
            SIMILAR_ARTISTS_KEY: 'similarartists_key',
            MOOD_GENRE_KEY: 'mood_genre_key',
            genresmoods: ["folk", "blues", "classical", "rock", "pop", "metal", "jazz", "soul", "punk", "singer-songwriter", "heavy metal", "hip-hop",  "reggae", "stoner rock", "country", "techno"]
    	};

    	return {
            get: function(){
                return data;
            },
            search: function(query, callbackSuccess, callbackError) {
                if(query){
                    if(this.isQueryGenreOrMood(query, this.get().genresmoods)){
                        this.getSimilar(query, true, callbackSuccess, callbackError);
                    }else{
                        this.getArtist(query, callbackSuccess, callbackError);
                        this.getSimilar(query, false, callbackSuccess, callbackError);
                    }
                }
            },
			getArtist: function(artistsNames, callbackSuccess, callbackError){
                var type = artistsNames.length > 1 ? data.ARTISTS_KEY : data.ARTIST_KEY;
                var thisOut = this;

                for (var i = 0; i < artistsNames.length; i++) {
                    var artistName = artistsNames[i];

                    artistName = this.scapeString(trimFast(artistName));
                    console.log("Artistname scaped: " + artistName);

                    Services.lastfm().getInfo(artistName, 
                        function(data){
                            //Success
                            var result = {
                                type: type,
                                data: data,
                                artistImages: data.image,
                                error: null
                            };
                            if(thisOut.atLeastOneImage(data.image)){
                                callbackSuccess(result, type);
                            }else{
                                callbackError("The artist supplied could not be found");
                            }

                        },function(error){
                            //Error
                            callbackError(error.message);
                        }); 
                }
			},
			getSimilar: function(queryItems, byTag, callbackSuccess, callbackError){
                var type = data.SIMILAR_ARTISTS_KEY;
                if(byTag) {
                    console.log("")
                    type = data.MOOD_GENRE_KEY;
                }
                var thisOut = this;

                if(queryItems.length <= 1){
                    var queryItem = trimFast(queryItems[0]);
                    console.log("Searching: " + queryItem);

                    var getSimilarFn = Services.lastfm().getSimilar;
                    if(byTag){
                        getSimilarFn = Services.lastfm().getByTag;
                    }

                    getSimilarFn(queryItem, 
                        function(result){
                            //Success
                            var arrayResult = []; 
                            for (var i = 0; i < result.length - 1; i++) {
                                if(thisOut.atLeastOneImage(result[i].image)){
                                    arrayResult.push(new SimilarArtist(result[i].name, result[i].url, result[i].image));
                                }
                            };
                            if(arrayResult.length > 0){
                                var result = {
                                    type: type,
                                    data: arrayResult
                                }

                                if(byTag){
                                    result.selectedMoodOrGenre = queryItem;
                                }
                                callbackSuccess(result);
                            }else{
                                callbackError("No similar artists");
                            }
                        },function(error){
                            //Error
                            callbackError(error.message);
                        });      

                }else{
                    var similarArtists = new Array();
                    for (var i = 0; i < queryItems.length; i++) {
                        var queryItem = trimFast(queryItems[i]);
                        console.log("Searching: " + queryItem);

                        var getSimilarFn = Services.lastfm().getSimilar;
                        if(byTag){
                            getSimilarFn = Services.lastfm().getByTag;
                        }

                        getSimilarFn(queryItem, 
                            function(result){
                                //Success
                                var arrayResult = []; 
                                for (var i = 0; i < result.length - 1; i++) {
                                    arrayResult.push(new SimilarArtist(result[i].name, result[i].url, result[i].image));
                                };

                                similarArtists.push(arrayResult);
                                if(similarArtists.length == queryItems.length){
                                    var resultCoindicences = thisOut.searchForCoindicences(queryItems, similarArtists);
                                    if(resultCoindicences[0] == "Error"){
                                        callbackError(resultCoindicences[1]);
                                    }else{
                                        var result = {
                                            type: type,
                                            data: resultCoindicences[1]
                                        }
                                        callbackSuccess(result);
                                    }
                                }
                            },function(error){
                                //Error
                                console.log(error.message);
                            });                    
                    };
                }
				
			},
            atLeastOneImage: function(images){
                for (var i = 0; i < images.length; i++) {
                     if(images[i]['#text'].length > 1){
                        return true;
                     }
                }
                return false;
            },
            searchForCoindicences: function (searchedArtists, similarArtists){
                //Lista en la que guardaremos los artista similares a varios artistas introducidos
                var artistasSimilaresAVarios = new Array();
                var artistasConSimilaresComunes = new Array();

                for (var a = 0; a < searchedArtists.length; a++) {
                    artistasConSimilaresComunes.push(false);
                }

                    for(var i = 0; i < similarArtists.length; i++) {
                        var artistasA = similarArtists[i];

                            for(var k = 0; k < artistasA.length-1; k++) {
                                var artistaA = artistasA[k];
                                var estaEnListas = new Array();

                                    for(var j = 0; j < similarArtists.length; j++) {
                                        if (i!=j) {
                                            var artistasB = similarArtists[j];

                                            var estaEnListaActual = false;
                                            for (var l = 0; l < artistasB.length-1; l++) {
                                                var artistaB = artistasB[l]
                                                if (artistaA.name == artistaB.name) {
                                                        estaEnListas.push(true);
                                                        estaEnListaActual = true;
                                                        //Esto es para saber que artistas tienen similares comunes a los demas introducidos por el usuario
                                                        artistasConSimilaresComunes[j] = true;
                                                }
                                            };
                                            if (!estaEnListaActual) {
                                                estaEnListas.push(false);
                                            };
                                        }
                                    };
                            
                                //Ahora comprobamos que el usuario este en todas las listas
                                var estaEnTodas = true;
                                for (var m = 0; m < estaEnListas.length; m++) {
                                    if (estaEnListas[m] != true){
                                        estaEnTodas = false;
                                    }
                                };
                                if (estaEnTodas) {
                                    if (!this.isArtistInList(artistaA, artistasSimilaresAVarios)){
                                        artistasSimilaresAVarios.push(artistaA);
                                    }
                                }
                            };
                    };

                    //Ahora comprobamos que las listas tengan algun artista en comun con las demás
                    var seQuedanFuera = new Array();
                    var seQuedanDentro = new Array();
                    for (var n = 0; n < artistasConSimilaresComunes.length; n++) {
                        if(artistasConSimilaresComunes[n] == false){
                            seQuedanFuera.push(similarArtists[n][similarArtists[n].length-1]);
                        }else{
                            seQuedanDentro.push(similarArtists[n][similarArtists[n].length-1]);
                        }
                    }

                    //Comprobamos que haya algun artista que añadir
                    if (artistasSimilaresAVarios.length>0) {
                        //Primero borraremos a los artistas introducidos de la lista de artistas similares
                        artistasSimilaresAVarios = this.removeSearchedArtists(searchedArtists, artistasSimilaresAVarios);

                        //Hacemos la misma comprobación despues de quitar los artistas introducidos del resultado
                        if (artistasSimilaresAVarios.length>0) {
                            console.log("artistasSimilaresAVarios");
                            console.log(artistasSimilaresAVarios);
                            return ["OK", artistasSimilaresAVarios];                         
                        }else{
                            //Comprobamos que clase de error debemos mostrar en relacion con los artistas introducidos y sus artistas similares
                            return(["Error", "My dear friend, those bands have nothing in common!"]);
                        }
                    }else{
                        //Comprobamos que clase de error debemos mostrar en relacion con los artistas introducidos y sus artistas similares
                        return(["Error", "My dear friend, those bands have nothing in common!"]);
                    }
            },
            isQueryGenreOrMood: function(query, list){
                for (var i = 0; i < list.length; i++) {
                    if (list[i] == query) {
                        return true;
                    }
                }    
                return false;
            },
            isArtistInList: function(artist, list){
                var isIn = false;
                for (var i = 0; i < list.length; i++) {
                    var artistAux = list[i];
                    if (artistAux.name == artist.name) {
                        isIn = true;
                        break;
                    }
                }    
                return isIn;
            },
            removeSearchedArtists: function (searchedArtists, list){
                for (var i = 0; i < searchedArtists.length; i++) {
                    var artistToDelete = searchedArtists[i];

                    for (var j = 0; j < list.length; j++) {
                        var  artistAux = list[j];
                        if (artistAux.name.toUpperCase().replace(/\s/g, '') === artistToDelete.toUpperCase().replace(/\s/g, '')) {
                                list.splice(j, 1);
                        }
                    }
                }
                return list;
            },
            scapeString: function(artistName){
                return  encodeURIComponent(artistName);
            }
    	};
    }]);

angular
	.module('delrollode')
    .factory('Events', [ '$http', '$log' , function ($http, $log) {

    	var data = {

    	};

    	return {
			getEvents: function(place){

			}
    	};

    }]);

angular
	.module('delrollode')
    .factory('History', [ '$http', '$log' , function ($http, $log) {

    	var data = {

    	};

    	return {
			getHistory: function(){
				return JSON.parse(localStorage.getItem('delrolloHistory'));
			},
			addToHistory: function(artist){
				var history = JSON.parse(localStorage.getItem('delrolloHistory'));
					history.artists.push(artist);
				localStorage.setItem('delrolloHistory', JSON.stringify(history));
			},
			deleteHistory: function(){
				localStorage.removeItem('delrolloHistory');
			}
    	};

    }]);



    function trimFast (str) {
        str = str.replace(/^\s+/, '');
        for (var i = str.length - 1; i >= 0; i--) {
            if (/\S/.test(str.charAt(i))) {
                str = str.substring(0, i + 1);
                break;
            }
        }
        return str;
    }
