'use strict';

angular
.module('delrollode')
.controller('MainCtrl', ['$scope', '$routeParams', '$location', '$interval', '$mdSidenav',
	function ($scope, $routeParams, $location, $interval, $mdSidenav) {

		adjustHeight();

		$scope.closeDetail = function(){
			$mdSidenav('right').close();
		}

	}
]);
