'use strict';

angular
.module('delrollode')
.controller('SearchCtrl', ['$scope', '$routeParams', '$location', 'Music', '$interval', '$mdSidenav', '$rootScope', '$sce', 
	function ($scope, $routeParams, $location, Music, $interval, $mdSidenav, $rootScope, $sce) {

		$interval(function(){
			adjustHeight();
		}, 200);

		$scope.searchFocus = false;
		$scope.artistsResult = [];
		$scope.multipleArtists = [];
		$scope.similarArtist = [];
		$scope.genresmoods = Music.get().genresmoods;

		var searchTerm = ($routeParams.searchTerm || "");
		if(searchTerm && searchTerm.length > 0){
			$scope.searching = true;
			$scope.emptySearch = false;
			console.log(searchTerm);
			$scope.query = searchTerm; //This is for the user to see the previous search in the search box
			

			//Element searched
			var searchItems = searchTerm.split(',');
			Music.search(searchItems, 
				function(result){
					console.log("Success: ");
					console.log(result);

					//Success
					$scope.mode = result.type;
					switch (result.type){
					    case Music.get().ARTIST_KEY:
					    	$scope.multipleArtists.push(result.data);
					    	break;
					    case Music.get().ARTISTS_KEY:
					    	$scope.multipleArtists.push(result.data);
				    		break;
				    	case Music.get().SIMILAR_ARTISTS_KEY:
				    		$scope.searching = false;
							$scope.similarArtist = result.data;
				    		break;
						case Music.get().MOOD_GENRE_KEY:
				    		$scope.searching = false;
							$scope.similarArtist = result.data;
							$scope.isMoodOrGenre = true;
							$scope.selectedMoodOrGenre = result.selectedMoodOrGenre;
				    		break;
				    }
				},function(result){
					//Error
					console.log(result);
					if(!$scope.searchError || $scope.searchError.length < 1){
						$(".search-error").css("display", "true");
						$scope.searchError = result;
						$(".search-error").velocity({ scaleX: .2, scaleY: .2}, { duration: 0 })
					 	.velocity({ scaleX: 1, scaleY: 1, opacity: 1}, 600, [500, 35]);
					}
					
					
				});
		}else{
			$scope.searching = false;
			$scope.emptySearch = true;
		}

		$scope.search = function(){
			if($scope.query &&  $scope.query.length > 0){
				$location.path("/Search/" + $scope.query + "/");
			}else{
				$location.path("/Search/");
				$scope.emptySearch = true;
			}
		}

		$scope.checkArtist = function(artist){
			console.log(artist);
			Music.getArtist([artist.name], 
				function(result){
					console.log(result);
					//Success
					$scope.mode = result.type;
					var bio = result.data.bio.content.replace(/\n/g, '<br />');
					bio = bio.slice(0, bio.indexOf("<a"));
					result.data.bio.content = $sce.trustAsHtml(bio);
					switch (result.type){
					    case Music.get().ARTIST_KEY:
					    	$rootScope.artistDetail = result;
					    	break;
					    default:
				    		break;
				    }
				    $mdSidenav('right').toggle();
				},function(result){
					//Error
			
				});
			
		}
	}
]);
