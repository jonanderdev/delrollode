

//Artist
function Artist(artistName, lastfmUrl, images){
	this.name = artistName;
	this.url = lastfmUrl;
	this.images = images || [];
}

//SimilarArtist
function SimilarArtist(artistName, lastfmUrl, images){
	this.name = artistName;
	this.url = lastfmUrl;
	this.images = (images) ? [images[2]['#text'], images[3]['#text'], images[4]['#text']] : [];
}
