'use strict';

angular
.module('delrollode', ['ngRoute','pascalprecht.translate', 'ngMaterial', 'ngTouch']);

angular
  .module('delrollode')
  .config(['$routeProvider', function (routeProvider) {
    routeProvider
      .when('/Search', {
        templateUrl: 'partials/main.html',
      })
      .when('/Search/:searchTerm', {
        templateUrl: 'partials/main.html',
      })
      .when('/Events', {
        templateUrl: 'partials/events.html',
      })
      .when('/History', {
        templateUrl: 'partials/history.html',
      })
      .otherwise({
        redirectTo: '/Search'
      });
  }]);

//Define the init function for starting up the application
angular
    .element(document)
    .ready(function() {
        if (window.jQuery) {
          //jquery loaded
          $("body").velocity({ opacity: 1 });
        }
    });


function adjustHeight(){
  var panelHeight = $(window).height();
  var windowWidth = $(window).width();
  if($('.viewContainer').height() != panelHeight){
    $('.cpanels, .cpanel, .viewContainer').height(panelHeight);
    var topPanelHeight = $('.top-bar').outerHeight();
  }
  $('.artist-content').height(panelHeight - 250 - 82); //window height - titleoverlay height - bottombar height

}




